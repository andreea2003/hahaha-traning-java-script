let shop = document.getElementById("shop");

/*
Obiectul document reprezintă documentul HTML în JavaScript 
care se afișează în fereastra din 
pagina web a browserului care, este vazut ca un obiect, de tip document,
cu mai multe metode, dintre care cele mai importante sunt 
- getElementById();
- getElementByClassName();
- getElementsByTagName();

*/

console.log(shop);

const input = document.getElementById("userInput");

let shopItemsData = [
  //array de obiecte
  {
    id: "jfhgbvnscs",
    name: "Jeleuri Croco",
    price: 45,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-1.jpeg",
  },
  {
    id: "ioytrhndcv",
    name: "Bratara de Crocodil",
    price: 100,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-2.jpeg",
  },
  {
    id: "wuefbncxbsn",
    name: "Croco de ciocolata",
    price: 25,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-3.jpeg",
  },
  {
    id: "bfwrbfrkefberu",
    name: "Croco Planta",
    price: 300,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-4.jpeg",
  },
  {
    id: "thiecbawdjksadjk",
    name: "Schelete Croco Verde",
    price: 25,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-5.jpeg",
  },
  {
    /* proprietate: valoare,*/ id: "iuertrywebncdjksadjk",
    name: "Schelete Croco Roz",
    price: 200,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-6.jpeg",
  },
  {
    id: "thierytbvcbvzdhadjk",
    name: "Gummy Croco",
    price: 450,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-7.jpeg",
  },
  {
    id: "trfoiwfcnbcawdjksadjk",
    name: "Biscuiti Croco",
    price: 45,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-8.jpeg",
  },
  {
    id: "cbvxbcvsceldk",
    name: "Croco Plusul",
    price: 85,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-9.jpeg",
  },
  {
    id: "oiopijmjkhuihb",
    name: "Croco scarpinatori",
    price: 120,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-10.jpeg",
  },
  {
    id: "oiopijewyiohbjhib",
    name: "Croco Caciula",
    price: 35,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-11.jpeg",
  },
  {
    id: "rtytytuyuytyytbvncv",
    name: "Croco Tricotatul",
    price: 350,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/img-12.jpg",
  },
];

let bag = [];

let generateShop = () => {
  //arrow function
  return (shop.innerHTML = shopItemsData
    /*
      	innerHTML ->Proprietatea InnerHTMLeste utilizată pentru
        modificarea conținutului HTML din codul JavaScript.
        Proprietatea InnerHTML este, de asemenea, utilizat
        pentru scrierea de conținut html dinamic pe documente HTML
    */
    .map((x) => {
      let { id, name, price, img, desc } = x;
      /*destructuring -> o expresie JavaScript care face posibilă
      separarea proprietăților din obiecte în variabile distincte*/

      /* 
      Backticks sunt literali delimitați cu caractere backtick (), 
      permițând șiruri de linii multiple
      Sunt utilizate cel mai frecvent pentru interpolarea șirurilor 
      (pentru a crea șiruri făcând înlocuirea substituenților)
      */
      return `<div id=product-id-${id} class="item ${name}">
  <img width="220" height="200" src=${img} alt="" />
  <div class="details">
    <h3 class="product">${name}</h3>
    <p>
      ${desc}
    </p>
    <div class="price-quantity">
      <h2>$ ${price}</h2>
      <div class="buttons">
        <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
        <div id=${id} class="quantity">0</div>
        <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
      </div>
    </div>
  </div>
</div>
    `;
    })
    .join(""));
  /*
    metoda returneaza un array de string-uri
    metoda nu modifica array ul origianl
    se poate folosi orice separator, iar default-ul e virgula : (,)
  */
};

generateShop();

let increment = (id) => {
  let selectedItem = id;
  let semafor = bag.find((x) => x.id === selectedItem.id);
  /*
  Metoda find returnează primul element din array, 
  care satisface funcția de testare furnizată. 
  Dacă nu există valori care să satisfacă funcția de testare,
  este by deafult returnat undefined. 
  */
  if (semafor === undefined) {
    bag.push({
      id: selectedItem.id,
      quantity: 1,
    });
  } else {
    semafor.quantity += 1;
  }
  console.log(bag);
  update(selectedItem.id);
};

let decrement = (id) => {
  let selectedItem = id;
  let semafor = bag.find((x) => x.id === selectedItem.id);
  if (semafor.quantity === 0) return;
  else {
    semafor.quantity -= 1;
  }
  update(selectedItem.id);
};

let update = (id) => {
  let semafor = bag.find((x) => x.id === id);
  document.getElementById(id).innerHTML = semafor.quantity;
  calculation();
};

let search_item = () => {
  let input = document.getElementById("searchbar").value;
  input = input.toLowerCase();
  //prelucreaza sirul de caractere astfel incat sa transforme fiecare caracter inttr-unul mic
  let x = document.getElementsByClassName("product");
  let items = document.getElementsByClassName("item");

  for (i = 0; i < x.length; i++) {
    if (!x[i].innerHTML.toLowerCase().includes(input)) {
      //functia includes verifica daca ceea ce am introdus in input se regaseste in numele item-ului selectat
      items[i].style.display = "none"; //modific proprietatile css, utilizand proprietatea style
    } else {
      items[i].style.display = "block";
    }
  }
};

search_item();

let calculation = () => {
  let cartIcon = document.getElementById("cartAmount");
  cartIcon.innerHTML = bag.map((x) => x.quantity).reduce((x, y) => x + y, 0);
  // functia reduce transforma valorile date de un array intr-o singura valoare,
  // utilizandu-se de functia data ca parametru;
  // al doilea paramtru este valoarea initiala, de la care pleaca x ul
};

let schimb = document.getElementById("toggleDark");
let body = document.querySelector("body");

schimb.addEventListener("click", function () {
  //un event listener primeste ca parametru tipul evenimentului si
  //functia care aplica modificari asupra paginii
  //this -> elementul pe care am aplicat eventlistener-ul
  this.classList.toggle("bi-moon");
  //proprietatea classList -> returneaza clasele CSS pe care elementul respectiv le are aplicate
  //proprietatea toogle -> adauga clasaa daca nu este deja aplicata si elimina clasa daca este deja aplicata
  console.log(this);
  if (this.classList.toggle("bi-brightness-high-fill")) {
    body.style.background = "#a5b3a6";
    body.style.color = "black";
    body.style.transition = "2s";
  } else {
    body.style.background = "black";
    body.style.color = "#a5b3a6";
    body.style.transition = "2s";
  }
});
